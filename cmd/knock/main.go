package main

import (
	"gitlab.com/liftedkilt/knock/pkg/config"
	"gitlab.com/liftedkilt/knock/pkg/knock"
)

func main() {
	c := config.Parse()

	knock.Execute(c)

}
