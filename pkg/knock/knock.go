package knock

import (
	"fmt"
	"net"
	"strconv"
	"sync"
	"time"

	"gitlab.com/liftedkilt/knock/pkg/config"
)

// Execute is the core app logic that handles dispatching all goroutines and compiling results
func Execute(c config.Config) {
	var wg sync.WaitGroup
	results := make(chan string, len(c.Hosts))

	// Dispatch Goroutines to check for liveliness
	for _, host := range c.Hosts {
		go knockConcurrently(host, results, &wg)
		wg.Add(1)
	}

	// Wait for all goroutines to return
	wg.Wait()
	// Close channel
	close(results)

	for line := range results {
		fmt.Println(line)
	}

}

func knock(h config.Host) bool {
	remote := h.Host + ":" + strconv.Itoa(h.Port)
	t := time.Duration(h.Timeout) * time.Millisecond
	var status bool

	conn, err := net.DialTimeout(h.Proto, remote, t)
	if err != nil {
		status = false
	} else {
		status = true
		defer conn.Close()
	}
	return status
}

func knockConcurrently(h config.Host, results chan string, wg *sync.WaitGroup) {
	result := knock(h)
	var status string

	if result {
		status = h.Host + " up"
	} else {
		status = h.Host + " down"
	}
	wg.Done()
	results <- status
}
