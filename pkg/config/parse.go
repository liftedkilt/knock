package config

// Config is a struct to hold all the Host structs
type Config struct {
	Hosts []Host
}

// Host is a struct to hold the parameters needed for an individual host
type Host struct {
	Host    string
	Port    int
	Proto   string
	Timeout int
}

// Parse takes no parameters, and returns a struct of type config with all parameters filled
func Parse() (c Config) {

	// temp test data
	hosts := []string{
		"127.0.0.1",
		"8.8.8.8",
		"192.168.86.254",
	}
	proto := "tcp"
	port := 22
	timeout := 500

	c = populateHosts(hosts, proto, port, timeout)
	return
}

func populateHosts(host []string, proto string, port, timeout int) (c Config) {
	for _, h := range host {
		newHost := Host{
			Host:    h,
			Port:    port,
			Proto:   proto,
			Timeout: timeout,
		}
		c.Hosts = append(c.Hosts, newHost)
	}
	return
}
