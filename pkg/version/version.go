package version

import (
	"io/ioutil"
	"net/http"
	"strings"
)

func Current() string {
	return "1.0.0"
}

func IsLatest(local, latest string) bool {
	if strings.Trim(latest, " \r\n") != strings.Trim(local, " \r\n") {
		return false
	}
	return true
}

func Latest(source string) (string, error) {
	resp, err := http.Get(source)

	defer resp.Body.Close()
	bytes, _ := ioutil.ReadAll(resp.Body)
	return string(bytes), err
}
