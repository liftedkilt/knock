package version

import (
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"net/http/httptest"
	"os"
	"testing"
)

func readVersionFromVersionFile() string {
	file, err := os.Open("../../configs/version")
	if err != nil {
		log.Fatal(err)
	}
	bytes, _ := ioutil.ReadAll(file)

	return string(bytes)
}

func TestCurrent(t *testing.T) {
	actualResult := Current()
	expectedResult := readVersionFromVersionFile()
	if actualResult != expectedResult {
		t.Fatalf("Expected %s but got %s", expectedResult, actualResult)
	}

}

func TestIsLatest(t *testing.T) {
	local, latest := "1.10.0", "1.10.0"
	if !IsLatest(local, latest) {
		t.Fatalf("Expected %s to equal %s", local, latest)
	}

	local, latest = "1.10.0", "1.10.0 "
	if !IsLatest(local, latest) {
		t.Fatalf("Expected %s to equal %s", local, latest)
	}

	local, latest = "1.10.0", "1.10.0 \n"
	if !IsLatest(local, latest) {
		t.Fatalf("Expected %s to equal %s", local, latest)
	}

	local, latest = "1.10.0", "9.99.9"
	if IsLatest(local, latest) {
		t.Fatalf("Expected %s to NOT equal %s", local, latest)
	}

	local, latest = "1.10.0", "1.1000"
	if IsLatest(local, latest) {
		t.Fatalf("Expected %s to NOT equal %s", local, latest)
	}

}

func TestLatest(t *testing.T) {
	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		fmt.Fprintf(w, `1.10.0`)
	}))
	defer ts.Close()

	actualResult, _ := Latest(ts.URL)
	expectedResult := "1.10.0"

	if actualResult != expectedResult {
		t.Fatalf("Expected '%s' but got '%s'", expectedResult, actualResult)
	}

}
