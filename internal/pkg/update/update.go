package update

import (
	"log"
	"net/http"

	update "github.com/inconshreveable/go-update"
)

func Do(url string) error {
	resp, err := http.Get(url)
	if err != nil {
		log.Fatalln(err)
	}
	defer resp.Body.Close()

	err = update.Apply(resp.Body, update.Options{})
	if err != nil {
		log.Fatalln(err)
	}

	return err
}
